import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Scanner;
import java.util.stream.IntStream;

public class BlockBootstrap {

	// Sum of a subarray, based on B(x, i, L) -- i is one-indexing
	public static double sum(double[] x, int i, int L) {
		double s = 0;
		int hi = i + L;
		while (i < hi) {
			s += x[i - 1];
			i++;
		}
		return s;
	}

	public static double mean(double[] x, int i, int L) {
		return sum(x, i, L)/((double) L);
	}


	// Compute MBB mean
	public static double mbbMu(double[] x, int L) {	
		return IntStream.range(0, x.length - L + 1)
				.parallel()
				.mapToDouble(idx -> mean(x, idx + 1, L))
				.average()
				.orElse(0);
	}

	// Compute MBB variance
	public static double mbbVariance(double[] x, int L, double alpha) {
		double mu = mbbMu(x, L);
		double lAlph = Math.pow(L, alpha);

		return IntStream.range(0, x.length - L + 1)
				.parallel()
				.mapToDouble(idx -> (lAlph * Math.pow(mean(x, idx + 1, L) - mu, 2)))
				.average()
				.orElse(0);
	}

	// Compute NBB mean
	public static double nbbMu(double[] x, int L) {
		int b = x.length / L;
		double muOut = 0;
		for (int i = 0; i < b; i++)
			muOut += mean(x, 1 + ((i + 1) - 1) * L, L);
		return muOut  / ((double) b);
	}


	// Compute NBB variance
	public static double nbbVariance(double[] x, int L, double alpha) {
		double mu = nbbMu(x, L);
		int b = x.length / L;
		double varSum = 0;
		for (int i = 0; i < b; i++)
			varSum += Math.pow(mean(x, 1 + ((i + 1) - 1) * L, L) - mu, 2);
		return Math.pow((double) L, alpha) * varSum / ((double) b);
	}


	// factorial implementation
	public static double factorial(int x) {
		double[] fact = {1.0, 1.0, 2.0, 6.0, 24.0, 120.0, 720.0, 5040.0, 40320.0, 362880.0, 3628800.0};
		return fact[x];
	}

	// Hermite polynomial
	public static double H(double x, int p) {
		double out = 0;
		for (int i = 0; i < (p / 2) + 1; i++) {
			out += Math.pow(-1, i) * Math.pow(x, p - (2 * i)) / 
					((factorial(i) * factorial(p - (2 * i))) * (1L << i));
		}
		out *= factorial(p);
		return out;
	}

	public static double[] rowMeans(double[][] x, int nrows, int ncols) {
		double[] means = new double[nrows];
		for (int i = 0; i < nrows; i++) {
			means[i] = mean(x[i], 1, ncols);
		}
		return means;
	}


	public static void duration(long start, long end) {
		System.out.println("Total execution time: " + (((double)(end - start))/60000) + " minutes");
	}


	public static void main(String[] argv) throws InterruptedException, IOException {

		System.out.println("Begin program... ");
		final long init = System.currentTimeMillis();

		// argv[0] is the file input name
		// argv[1] is the MBB file output name
		// argv[2] is the NBB file output name
		// argv[3] is the number of rows (each an individual sample)
		// argv[4] is the number of columns (for the length of the time series)
		// argv[5] is the maximum desired block length
		// argv[6] is p, in the Hermite polynomial H_p(x)
		// argv[7] is alpha, the long-memory parameter, in (0, 1)
		// argv[8] is the means file name
		// argv[9] is the (inclusive) first row
		// argv[10] is the (exclusive) last row

		DataInputStream fileIn = new DataInputStream(new BufferedInputStream(new FileInputStream(argv[0])));
		RandomAccessFile fileOutMBB = new RandomAccessFile(argv[1], "rw");
		RandomAccessFile fileOutNBB = new RandomAccessFile(argv[2], "rw");
		RandomAccessFile fileOutMean = new RandomAccessFile(argv[8], "rw");

		Scanner scnr = new Scanner(fileIn);

		// These variables are taken from the command line, but are inputted here for ease of use.
		int rows = Integer.parseInt(argv[3]);
		int cols = Integer.parseInt(argv[4]);
		int maxBlockSize = Integer.parseInt(argv[5]);
		int p = Integer.parseInt(argv[6]);
		int firstRow = Integer.parseInt(argv[9]);
		int lastRow = Integer.parseInt(argv[10]);
		double alpha = Double.parseDouble(argv[7]);
		double[][] oldSeries = new double[rows][cols];

		FileChannel outFSMBBChan = fileOutMBB.getChannel();
		FileChannel outFSNBBChan = fileOutNBB.getChannel();
		FileChannel outFSmeanChan = fileOutMean.getChannel();

		// double is 8 bytes
		ByteBuffer outFSMBB = outFSMBBChan.map(FileChannel.MapMode.READ_WRITE, 0, 8 * (lastRow - firstRow) * maxBlockSize);
		ByteBuffer outFSNBB = outFSNBBChan.map(FileChannel.MapMode.READ_WRITE, 0, 8 * (lastRow - firstRow) * maxBlockSize);
		ByteBuffer outFSmean = outFSmeanChan.map(FileChannel.MapMode.READ_WRITE, 0, 8 * rows);

		// read in the file, and perform the H_p(x) transformation		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				oldSeries[i][j] = fileIn.readDouble();
			}
		}

		double[][] timeSeries = IntStream.range(0, rows)
				.parallel()
				.mapToObj(i -> IntStream.range(0, cols)
						.parallel()
						.mapToDouble(j -> (H(oldSeries[i][j], p)))
						.toArray()
						).toArray(double[][]::new);

		// row means
		double[] sampleMeans = rowMeans(timeSeries, rows, cols);
		for (int i = 0; i < rows; i++) {
			outFSmean.putDouble(sampleMeans[i]);
		}
		outFSmeanChan.close();

		double[][] outNBB = IntStream.range(firstRow, lastRow)
				.parallel()
				.mapToObj(j -> IntStream.range(0, maxBlockSize)
						.parallel()
						.mapToDouble(m -> (nbbVariance(timeSeries[j], m + 1, alpha)))
						.toArray()
						).toArray(double[][]::new);

		double[][] outMBB = IntStream.range(firstRow, lastRow)
				.parallel()
				.mapToObj(j -> IntStream.range(0, maxBlockSize)
						.parallel()
						.mapToDouble(m -> (mbbVariance(timeSeries[j], m + 1, alpha)))
						.toArray()
						).toArray(double[][]::new);


		for (int j = firstRow; j < lastRow; j++) {
			for (int m = 0; m < maxBlockSize; m++) {
				outFSMBB.putDouble(outMBB[j][m]);
				outFSNBB.putDouble(outNBB[j][m]);
			}
		}

		outFSMBBChan.close();
		outFSNBBChan.close();

		fileOutMBB.close();
		fileOutNBB.close();
		fileOutMean.close();
		fileIn.close();

		System.out.println("Total time... ");
		duration(init, System.currentTimeMillis());
	}
}

