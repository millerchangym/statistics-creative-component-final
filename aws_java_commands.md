# Amazon Web Services Directions

## Setup

* In Amazon Web Services (AWS) EC2:
    * Use OpenJDK 11 (Java 11) on Ubuntu 18.04. If you want to start with a new Ubuntu instance from scratch, see: 
https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04
    * Use c5d.24xlarge. 
    * Allow access to all buckets through Identity and Access Management (IAM). Make sure you download the key pair.
    * Assume ~32 GB of memory.
    * Open PuTTY (https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html).
    * Use `ubuntu@public_dns_name` as the host name, where `public_dns_name` is obtained from your running EC2 instance.
    * In PuTTY, go to Auth and select the .ppk file, generated using your key pair based on instructions in 
https://linuxacademy.com/guide/17385-use-putty-to-access-ec2-linux-instances-via-ssh-from-windows/.
    * If you get an error, check for processes that are running:  
    ```shell
    ps aux | grep -i apt
    ``` 
    and cancel them using the process ID as necessary:
    ```shell 
    sudo kill <process_id>
    ```

* Continuing in your Ubuntu instance run from EC2:
    * Install the Amazon Web Services (AWS) Command Line Interface (CLI): `sudo apt install awscli`
    * Upload all files in `DATA_FILES` to AWS S3. Take note of the S3 bucket name.
    * Run 
	```shell 
	aws s3 sync s3://[your-S3-bucket-name] .
	``` 
	to bring these into your Ubuntu instance.
    * Create the output directories: `mkdir MEANS MBB NBB`
    * Compile the Java file for the row mean, MBB variance, and NBB variance calculations: `javac BlockBootstrap.java`
    * Run the commands as provided in the next two sections. You should expect one command to take about 20-50 minutes to process.

### FARIMA calculations

```shell
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p1.txt 5000 3000 3000 1 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p2.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p2.txt 5000 3000 3000 2 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p2.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p3.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p3.txt 5000 3000 3000 3 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p3.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p4.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p4.txt 5000 3000 3000 4 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p4.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p5.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p5.txt 5000 3000 3000 5 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p5.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p6.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p6.txt 5000 3000 3000 6 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p6.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p7.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p7.txt 5000 3000 3000 7 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p7.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p8.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p8.txt 5000 3000 3000 8 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p8.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point1.txt ./MBB/farima_mbb_M5000_n3000_alpha0point1_k3000_p9.txt ./NBB/farima_nbb_M5000_n3000_alpha0point1_k3000_p9.txt 5000 3000 3000 9 0.1 ./MEANS/farima_means_M5000_n3000_alpha0point1_p9.txt 0 5000

java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point2.txt ./MBB/farima_mbb_M5000_n3000_alpha0point2_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point2_k3000_p1.txt 5000 3000 3000 1 0.2 ./MEANS/farima_means_M5000_n3000_alpha0point2_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point2.txt ./MBB/farima_mbb_M5000_n3000_alpha0point2_k3000_p2.txt ./NBB/farima_nbb_M5000_n3000_alpha0point2_k3000_p2.txt 5000 3000 3000 2 0.2 ./MEANS/farima_means_M5000_n3000_alpha0point2_p2.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point2.txt ./MBB/farima_mbb_M5000_n3000_alpha0point2_k3000_p3.txt ./NBB/farima_nbb_M5000_n3000_alpha0point2_k3000_p3.txt 5000 3000 3000 3 0.2 ./MEANS/farima_means_M5000_n3000_alpha0point2_p3.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point2.txt ./MBB/farima_mbb_M5000_n3000_alpha0point2_k3000_p4.txt ./NBB/farima_nbb_M5000_n3000_alpha0point2_k3000_p4.txt 5000 3000 3000 4 0.2 ./MEANS/farima_means_M5000_n3000_alpha0point2_p4.txt 0 5000

java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point3.txt ./MBB/farima_mbb_M5000_n3000_alpha0point3_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point3_k3000_p1.txt 5000 3000 3000 1 0.3 ./MEANS/farima_means_M5000_n3000_alpha0point3_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point3.txt ./MBB/farima_mbb_M5000_n3000_alpha0point3_k3000_p2.txt ./NBB/farima_nbb_M5000_n3000_alpha0point3_k3000_p2.txt 5000 3000 3000 2 0.3 ./MEANS/farima_means_M5000_n3000_alpha0point3_p2.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point3.txt ./MBB/farima_mbb_M5000_n3000_alpha0point3_k3000_p3.txt ./NBB/farima_nbb_M5000_n3000_alpha0point3_k3000_p3.txt 5000 3000 3000 3 0.3 ./MEANS/farima_means_M5000_n3000_alpha0point3_p3.txt 0 5000

java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point4.txt ./MBB/farima_mbb_M5000_n3000_alpha0point4_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point4_k3000_p1.txt 5000 3000 3000 1 0.4 ./MEANS/farima_means_M5000_n3000_alpha0point4_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point4.txt ./MBB/farima_mbb_M5000_n3000_alpha0point4_k3000_p2.txt ./NBB/farima_nbb_M5000_n3000_alpha0point4_k3000_p2.txt 5000 3000 3000 2 0.4 ./MEANS/farima_means_M5000_n3000_alpha0point4_p2.txt 0 5000

java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point5.txt ./MBB/farima_mbb_M5000_n3000_alpha0point5_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point5_k3000_p1.txt 5000 3000 3000 1 0.5 ./MEANS/farima_means_M5000_n3000_alpha0point5_p1.txt 0 5000

java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point6.txt ./MBB/farima_mbb_M5000_n3000_alpha0point6_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point6_k3000_p1.txt 5000 3000 3000 1 0.6 ./MEANS/farima_means_M5000_n3000_alpha0point6_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point7.txt ./MBB/farima_mbb_M5000_n3000_alpha0point7_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point7_k3000_p1.txt 5000 3000 3000 1 0.7 ./MEANS/farima_means_M5000_n3000_alpha0point7_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point8.txt ./MBB/farima_mbb_M5000_n3000_alpha0point8_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point8_k3000_p1.txt 5000 3000 3000 1 0.8 ./MEANS/farima_means_M5000_n3000_alpha0point8_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/farima_file_M5000_n3000_alpha0point9.txt ./MBB/farima_mbb_M5000_n3000_alpha0point9_k3000_p1.txt ./NBB/farima_nbb_M5000_n3000_alpha0point9_k3000_p1.txt 5000 3000 3000 1 0.9 ./MEANS/farima_means_M5000_n3000_alpha0point9_p1.txt 0 5000
```

### FGN calculations

```shell
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p1.txt 5000 3000 3000 1 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p2.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p2.txt 5000 3000 3000 2 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p2.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p3.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p3.txt 5000 3000 3000 3 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p3.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p4.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p4.txt 5000 3000 3000 4 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p4.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p5.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p5.txt 5000 3000 3000 5 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p5.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p6.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p6.txt 5000 3000 3000 6 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p6.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p7.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p7.txt 5000 3000 3000 7 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p7.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p8.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p8.txt 5000 3000 3000 8 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p8.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point1.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point1_k3000_p9.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point1_k3000_p9.txt 5000 3000 3000 9 0.1 ./MEANS/fgn_means_M5000_n3000_alpha0point1_p9.txt 0 5000

java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point2.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point2_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point2_k3000_p1.txt 5000 3000 3000 1 0.2 ./MEANS/fgn_means_M5000_n3000_alpha0point2_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point2.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point2_k3000_p2.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point2_k3000_p2.txt 5000 3000 3000 2 0.2 ./MEANS/fgn_means_M5000_n3000_alpha0point2_p2.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point2.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point2_k3000_p3.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point2_k3000_p3.txt 5000 3000 3000 3 0.2 ./MEANS/fgn_means_M5000_n3000_alpha0point2_p3.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point2.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point2_k3000_p4.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point2_k3000_p4.txt 5000 3000 3000 4 0.2 ./MEANS/fgn_means_M5000_n3000_alpha0point2_p4.txt 0 5000

java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point3.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point3_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point3_k3000_p1.txt 5000 3000 3000 1 0.3 ./MEANS/fgn_means_M5000_n3000_alpha0point3_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point3.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point3_k3000_p2.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point3_k3000_p2.txt 5000 3000 3000 2 0.3 ./MEANS/fgn_means_M5000_n3000_alpha0point3_p2.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point3.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point3_k3000_p3.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point3_k3000_p3.txt 5000 3000 3000 3 0.3 ./MEANS/fgn_means_M5000_n3000_alpha0point3_p3.txt 0 5000

java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point4.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point4_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point4_k3000_p1.txt 5000 3000 3000 1 0.4 ./MEANS/fgn_means_M5000_n3000_alpha0point4_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point4.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point4_k3000_p2.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point4_k3000_p2.txt 5000 3000 3000 2 0.4 ./MEANS/fgn_means_M5000_n3000_alpha0point4_p2.txt 0 5000

java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point5.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point5_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point5_k3000_p1.txt 5000 3000 3000 1 0.5 ./MEANS/fgn_means_M5000_n3000_alpha0point5_p1.txt 0 5000

java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point6.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point6_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point6_k3000_p1.txt 5000 3000 3000 1 0.6 ./MEANS/fgn_means_M5000_n3000_alpha0point6_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point7.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point7_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point7_k3000_p1.txt 5000 3000 3000 1 0.7 ./MEANS/fgn_means_M5000_n3000_alpha0point7_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point8.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point8_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point8_k3000_p1.txt 5000 3000 3000 1 0.8 ./MEANS/fgn_means_M5000_n3000_alpha0point8_p1.txt 0 5000
java BlockBootstrap ./DATA_FILES/fgn_file_M5000_n3000_alpha0point9.txt ./MBB/fgn_mbb_M5000_n3000_alpha0point9_k3000_p1.txt ./NBB/fgn_nbb_M5000_n3000_alpha0point9_k3000_p1.txt 5000 3000 3000 1 0.9 ./MEANS/fgn_means_M5000_n3000_alpha0point9_p1.txt 0 5000
```

* Continuing in your Ubuntu instance run from EC2:
    * Move all output files to S3 using 
	```shell 
	aws s3 sync . s3://[your-S3-bucket-name] 
	```
* Download all files to your Windows hard drive using the Windows Command Prompt with the AWS CLI (https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and configure using https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html. Execute 
	```shell 
	aws s3 sync s3://[your-S3-bucket-name] .
	``` 
    * I strongly recommend using an Ethernet connection and not a WiFi connection. You might possibly exceed your bandwidth allowed by your Internet Service Provider by doing this download. Make sure you do some searching on how to adjust this as appropriate for your needs. I am not responsible for problems that arise from doing this download.